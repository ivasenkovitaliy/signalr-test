﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Hosting;
using Server.Hubs;
using Server.Models;

namespace Server.HostedServices
{
    public class ObjectHostedService : IHostedService
    {
        private Timer _timer;
        private readonly IHubContext<SomeObjectHub> _hubContext;
        private const string MethodName= "ReceiveMessage";

        public ObjectHostedService(IHubContext<SomeObjectHub> hubContext)
        {
            _hubContext = hubContext;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _timer = new Timer(DoWork, null, TimeSpan.Zero,
                TimeSpan.FromSeconds(10));

            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        private void DoWork(object state)
        {
            var someObject = new NotASimpleModel
            {
                Id = Guid.NewGuid(),
                CreatedAt = DateTime.UtcNow,
                Data = GetRandomString()
            };

            _hubContext.Clients.All.SendAsync(MethodName, someObject);
        }


        private string GetRandomString()
        {
            var random = new Random();
            var allowedChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            
            return new string(Enumerable.Repeat(allowedChars, random.Next(10, 16))
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}

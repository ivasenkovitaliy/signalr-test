﻿using System;

namespace Server.Models
{
    public class NotASimpleModel
    {
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public string Data { get; set; }
    }
}

# Simple SignalR sample with non-simple object (like a string )))

## Development server

Run `docker-compose up -d` for a dev server. Navigate to `http://localhost:8380/`. The app will display received messages from simple server every 10 seconds. 
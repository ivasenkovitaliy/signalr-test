const state = {
    notASimpleObject: {
        id: '',
        createdAt: '',
        data: ''
    }
};

const getters = {
    notASimpleObject: state => state.notASimpleObject
};

const actions = {
    updateWith({ commit }, payload){
        commit('updateDataState', payload);
    }
};

const mutations = {
    updateDataState(state, payload){
        state.notASimpleObject = payload;
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}